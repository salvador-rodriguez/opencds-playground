[when] Assertion.Exists - There is assertion {ASSERTION:ENUM:Assertion.value} = (
	Assertion(value == "{ASSERTION}")
) /* DslUsed==Assertion.Exists.Dsl|||ASSERTION=={ASSERTION} */