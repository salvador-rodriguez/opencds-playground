[when] Assertion.Not.Exists - There is NOT an assertion {ASSERTION:ENUM:Assertion.value} = 
( java.util.List (size == 0) from accumulate
                ( 
                $TempId : Assertion
                                (
                                value == "{ASSERTION}" 
                                ),
                init (List $TempIds = new ArrayList(); ), action ($TempIds.add($TempId); ), reverse ($TempIds.remove($TempId); ), result($TempIds) 
                )
) /* DslUsed==Assertion.Not.Exists.Dsl|||ASSERTION=={ASSERTION} */
