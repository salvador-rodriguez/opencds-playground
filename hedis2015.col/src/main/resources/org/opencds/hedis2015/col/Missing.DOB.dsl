[when] Missing.DOB - Patient is missing date of birth = 
(
  (Patient(isIdentifierListContains(identifier, $subjectId), birthDate == null)) 
) /* DslUsed==Missing.DOB.Dsl */