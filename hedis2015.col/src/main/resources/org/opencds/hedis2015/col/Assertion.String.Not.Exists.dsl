[when] Assertion.String.Not.Exists - There is NOT an Assertion String {ASSERTIONSTRING} = (
   java.util.List (size == 0) from accumulate
                ( 
                $TempId : Assertion
                                (
                                value == "{ASSERTIONSTRING}" 
                                ),
                init (List $TempIds = new ArrayList(); ), action ($TempIds.add($TempId); ), reverse ($TempIds.remove($TempId); ), result($TempIds) 
                )
) /* DslUsed==Assertion.String.Not.Exists.Dsl|||ASSERTIONSTRING=={ASSERTIONSTRING} */