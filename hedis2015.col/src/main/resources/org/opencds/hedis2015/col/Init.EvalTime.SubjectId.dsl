[when] Init.EvalTime.SubjectId - Note that all criteria below must be met for the rule to fire. = 
( 
    Observation(isCodingContains(code, "http://snomed.info/sct", "129265001"), $evalTime : ((DateTimeDt) value).getValue())  /* SNOMED code = Evaluation */
    
    and 
    
    DataElement(name == "FocalPersonId", $subjectId : identifierFirstRep)   
) /* DslUsed==Init.EvalTime.SubjectId */  