[when] Assertion.String.Exists - There is an Assertion String {ASSERTIONSTRING} = (
	Assertion
	(
		value == "{ASSERTIONSTRING}"
	)
) /* DslUsed==Assertion.String.Exists|||ASSERTIONSTRING=={ASSERTIONSTRING} */
