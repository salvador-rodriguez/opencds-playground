[when] Proc.Past - Patient {COUNTMAYBEASTRUE:ENUM:EnumerationTarget.countMaybeAsTrue} {PROC:ENUM:ProcedureConcept.openCdsConceptCode} prior to Eval Time = (
	Procedure
	(
	    isDateTimeOrPeriodBefore(performed, $evalTime, {COUNTMAYBEASTRUE}),
	    subject != null,
	    subject.reference != null,
		$ProcLatPastDsl_subjectIdDt_{PROC} : subject.reference,
	)
	and
	Patient
	(
	    id != null,
	    id.value == $ProcLatPastDsl_subjectIdDt_{PROC}.value,
	    isIdentifierListContains(identifier, $subjectId)
	)
) /* DslUsed==Proc.Past.Dsl|||Proc=={PROC}|||HIGHLOW=={HIGHLOW} */